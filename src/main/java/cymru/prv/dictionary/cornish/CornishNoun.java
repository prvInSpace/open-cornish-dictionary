package cymru.prv.dictionary.cornish;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONObject;


/**
 * Represents a Cornish noun
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class CornishNoun extends CornishWord {

    public CornishNoun(JSONObject obj) {
        super(obj, WordType.noun);
    }

}
