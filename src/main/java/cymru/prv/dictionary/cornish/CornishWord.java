package cymru.prv.dictionary.cornish;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONObject;

/**
 * Represents a generic Cornish word.
 *
 * Used to override the getMutations method
 * of the Word class so that all Cornish words
 * get the mutation.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public abstract class CornishWord extends Word {


    /**
     * Creates an instance of the word.
     * The field normalForm is required and can not be empty.
     *
     * @param obj        The data for the word
     * @param type       The word type of the word
     * @throws IllegalArgumentException if normalForm is empty or null
     */
    public CornishWord(JSONObject obj, WordType type) {
        super(obj, type);
    }

    @Override
    protected JSONObject getMutations() {
        return Mutation.getMutations(getNormalForm());
    }
}
