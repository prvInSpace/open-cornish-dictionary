package cymru.prv.dictionary.cornish.tenses;

import cymru.prv.dictionary.cornish.CornishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class SubjunctiveImperfect extends Subjunctive {

    public SubjunctiveImperfect(CornishVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        if(endsInYa)
            return Collections.singletonList(apply("yen"));
        return Collections.singletonList(apply("en"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        if(endsInYa)
            return Collections.singletonList(apply("yes"));
        return Collections.singletonList(apply("es"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        if(endsInYa)
            return Collections.singletonList(apply("ya"));
        return Collections.singletonList(apply("a"));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        if(endsInYa)
            return Collections.singletonList(apply("yen"));
        return Collections.singletonList(apply("en"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        if(endsInYa)
            return Collections.singletonList(apply("yewgh"));
        return Collections.singletonList(apply("ewgh"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        if(endsInYa)
            return Collections.singletonList(apply("yens"));
        return Collections.singletonList(apply("ens"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(applyWithAffectation("ys"));
    }
}
