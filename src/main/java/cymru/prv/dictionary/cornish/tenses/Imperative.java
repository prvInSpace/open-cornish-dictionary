package cymru.prv.dictionary.cornish.tenses;

import cymru.prv.dictionary.cornish.CornishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class Imperative extends CornishVerbTense {

    public Imperative(CornishVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        if(endsInYa)
            return List.of(apply(""), apply("y"));
        return Collections.singletonList(apply(""));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        if(endsInYa)
            return Collections.singletonList(apply("yes"));
        return Collections.singletonList(apply("es"));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(applyWithAffectation("yn"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        if(endsInYa)
            return Collections.singletonList(apply("yewgh"));
        return Collections.singletonList(applyWithAffectation("ewgh"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        if(endsInYa)
            return Collections.singletonList(apply("yens"));
        return Collections.singletonList(apply("ens"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.emptyList();
    }
}
