package cymru.prv.dictionary.cornish.tenses;

import cymru.prv.dictionary.cornish.CornishVerb;
import org.json.JSONObject;

/**
 * A verb tense without no default
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class SpecialVerbTense extends CornishVerbTense {

    public SpecialVerbTense(CornishVerb verb, JSONObject obj) {
        super(verb, obj);
    }

}
