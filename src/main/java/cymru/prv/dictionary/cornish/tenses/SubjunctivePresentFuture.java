package cymru.prv.dictionary.cornish.tenses;

import cymru.prv.dictionary.cornish.Affection;
import cymru.prv.dictionary.cornish.CornishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class SubjunctivePresentFuture extends Subjunctive {

    public SubjunctivePresentFuture(CornishVerb verb, JSONObject obj) {
        super(verb, obj);
        raisedStem = verb.generateAffectionStem(stem, Affection.FULLY_RAISED);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.singletonList(applyWithAffectation("iv"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Collections.singletonList(applyWithAffectation("i"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        if(endsInYa)
            return Collections.singletonList(apply("yo"));
        return Collections.singletonList(apply("o"));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(applyWithAffectation("yn"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        if(endsInYa)
            return Collections.singletonList(apply("yowgh"));
        return Collections.singletonList(applyWithAffectation("owgh"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        if(endsInYa)
            return Collections.singletonList(apply("yons"));
        return Collections.singletonList(apply("ons"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        if(endsInYa)
            return Collections.singletonList(apply("yer"));
        return Collections.singletonList(apply("er"));
    }
}
