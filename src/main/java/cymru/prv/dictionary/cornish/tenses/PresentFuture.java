package cymru.prv.dictionary.cornish.tenses;

import cymru.prv.dictionary.cornish.Affection;
import cymru.prv.dictionary.cornish.CornishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class PresentFuture extends CornishVerbTense {

    private final CornishVerb verb;

    public PresentFuture(CornishVerb verb, JSONObject obj) {
        super(verb, obj);
        this.verb = verb;
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        if(endsInYa)
            return Collections.singletonList(apply("yav"));
        return Collections.singletonList(apply("av"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Collections.singletonList(applyWithAffectation("ydh"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        if(verb.getInfinitiveAffection() == Affection.RAISED)
            return Collections.singletonList(applyWithAffectation(""));
        return Collections.singletonList(apply(""));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(applyWithAffectation("yn"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        if(endsInYa)
            return Collections.singletonList(applyWithAffectation("yowgh"));
        return Collections.singletonList(applyWithAffectation("owgh"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        if(endsInYa)
            return Collections.singletonList(apply("yons"));
        return Collections.singletonList(apply("ons"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(applyWithAffectation("ir"));
    }
}
