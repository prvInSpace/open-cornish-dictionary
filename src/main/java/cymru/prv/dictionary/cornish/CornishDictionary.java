package cymru.prv.dictionary.cornish;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.DictionaryList;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONObject;

import java.util.Map;
import java.util.function.Function;

public class CornishDictionary extends Dictionary {

    private static final Map<WordType, Function<JSONObject, Word>> wordTypeFromJsonMap = Map.of(
            WordType.verb, CornishVerb::new,
            WordType.adjective, CornishAdjective::new,
            WordType.conjunction, (w) -> new Word(w, WordType.conjunction),
            WordType.noun, CornishNoun::new,
            WordType.adposition, CornishAdposition::new
    );

    public CornishDictionary(){
        super("kw", wordTypeFromJsonMap);
    }

    public CornishDictionary(DictionaryList dictionaryList) {
        super(dictionaryList, "kw", wordTypeFromJsonMap);
    }


    @Override
    public String getVersion() {
        return "1.2.0";
    }
}
