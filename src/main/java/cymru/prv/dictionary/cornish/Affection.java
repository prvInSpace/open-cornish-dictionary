package cymru.prv.dictionary.cornish;

public enum Affection {
    BASE(0), RAISED(1), FULLY_RAISED(2);

    public final int degree;

    Affection(int degree) {
        this.degree = degree;
    }
}
