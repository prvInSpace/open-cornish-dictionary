package cymru.prv.dictionary.cornish

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.common.json.Json
import cymru.prv.dictionary.cornish.tenses.SubjunctivePresentFuture
import org.json.JSONObject
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TestVerbAffections {

    @Test
    fun `test verbs with a-e-y affection`(){
        val verb = CornishVerb(JSONObject()
                .put("normalForm", "kara")
                .put("affection", "a/e/y")
        )
        assertEquals("kar", verb.stem)

        val conj = verb.conjugations

        // Present tense
        val pres = conj.getJSONObject("present_future")
        assertEquals("kerydh", Json.getStringList(pres, "singSecond")[0])
        assertEquals("keryn", Json.getStringList(pres, "plurFirst")[0])
        assertEquals("kerowgh", Json.getStringList(pres, "plurSecond")[0])

        // Preterite
        val pret = conj.getJSONObject("preterite")
        assertEquals("keris", Json.getStringList(pret, "singFirst")[0])
        assertEquals("kersys", Json.getStringList(pret, "singSecond")[0])

        // Since it does not have a -is ending it should not have an affection
        assertEquals("karas", Json.getStringList(pret, "singThird")[0])
        assertEquals("kersyn", Json.getStringList(pret, "plurFirst")[0])
        assertEquals("kersowgh", Json.getStringList(pret, "plurSecond")[0])

        // Imperfect

        // Subjunctive
        val presSub = conj.getJSONObject("subjunctive_present_future")
        assertEquals("kyrriv", Json.getStringList(presSub, "singFirst")[0])
        assertEquals("kyrri", Json.getStringList(presSub, "singSecond")[0])
        assertEquals("kyrryn", Json.getStringList(presSub, "plurFirst")[0])
        assertEquals("kyrrowgh", Json.getStringList(presSub, "plurSecond")[0])
    }

    @Test
    fun `test a-e verbs should not receive double affection`(){
        val verb = CornishVerb(JSONObject()
                .put("normalForm", "havi")
                .put("affection", "a/e")
        )
        assertEquals("hav", verb.stem)


        val presSub = verb.conjugations.getJSONObject("subjunctive_present_future")
        assertEquals("heffiv", Json.getStringList(presSub, "singFirst")[0])
        assertEquals("heffi", Json.getStringList(presSub, "singSecond")[0])
        assertEquals("heffyn", Json.getStringList(presSub, "plurFirst")[0])
        assertEquals("heffowgh", Json.getStringList(presSub, "plurSecond")[0])
    }

    @Test
    fun `test aa-ee-ay special case`(){
        val verb = CornishVerb(JSONObject()
                .put("normalForm", "dargana")
                .put("affection", "aa/ee/ay")
        )
        assertEquals("dargan", verb.stem)
        assertEquals("dergen", verb.getStem(Affection.RAISED))
        assertEquals("dargyn", verb.generateAffectionStem(verb.stem, Affection.FULLY_RAISED))

        val tense = SubjunctivePresentFuture(verb, JSONObject()).toJson()
        assertEquals("dargynniv", Json.getStringList(tense, "singFirst")[0])
    }

    @Test
    fun `words ending with -el should de-affect the stem`(){
        val verb = CornishVerb(JSONObject()
                .put("normalForm", "lemmel")
                .put("affection", "a/e")
        )
        assertEquals("lamm", verb.stem)
    }

    @Test
    fun `words ending with -el with several affected vowels should correctly de-affect the stem`(){
        val verb = CornishVerb(JSONObject()
                .put("normalForm", "darleverel")
                .put("affection", "aa/ee")
        )
        assertEquals("darlavar", verb.stem)
    }

    @Test
    fun `words with a-y affection should recieve affection`(){
        val verb = CornishVerb(JSONObject()
            .put("normalForm", "amma")
            .put("affection", "a/y")
        )
        assertEquals("ymm", verb.getStem(Affection.RAISED))

        val conj = verb.conjugations
        val pret = conj.getJSONObject("preterite")
        assertEquals("ymsyn", Json.getStringList(pret, "plurFirst")[0])
    }

     @Test
     fun `verbs using second form should receive affection in imperfect`(){
         val verb = CornishVerb(JSONObject()
             .put("normalForm", "lemmel")
             .put("affection", "a/e")
             .put("imperfect", JSONObject().put("useSecondForm", true))
         )
         assertEquals("lamm", verb.stem)
         assertEquals("lemm", verb.getStem(Affection.RAISED))

         val conj = verb.conjugations
         val imp = conj.getJSONObject("imperfect")

         assertEquals("lemmyn", Json.getStringList(imp, "singFirst")[0])
         assertEquals("lemmys", Json.getStringList(imp, "singSecond")[0])
         assertEquals("lemmi", Json.getStringList(imp, "singThird")[0])
         assertEquals("lemmyn", Json.getStringList(imp, "plurFirst")[0])
         assertEquals("lemmewgh", Json.getStringList(imp, "plurSecond")[0])
         assertEquals("lemmens", Json.getStringList(imp, "plurThird")[0])
         assertEquals("lemmys", Json.getStringList(imp, "impersonal")[0])
     }

    @Test
    fun `verbs not using second form should not receive affection in imperfect`(){
        val verb = CornishVerb(JSONObject()
            .put("normalForm", "lamma")
            .put("affection", "a/e/y")
        )
        assertEquals("lamm", verb.stem)
        assertEquals("lemm", verb.getStem(Affection.RAISED))

        val conj = verb.conjugations
        val imp = conj.getJSONObject("imperfect")

        assertEquals("lammen", Json.getStringList(imp, "singFirst")[0])
        assertEquals("lammes", Json.getStringList(imp, "singSecond")[0])
        assertEquals("lamma", Json.getStringList(imp, "singThird")[0])
        assertEquals("lammen", Json.getStringList(imp, "plurFirst")[0])
        assertEquals("lammewgh", Json.getStringList(imp, "plurSecond")[0])
        assertEquals("lammens", Json.getStringList(imp, "plurThird")[0])

        // Imperfect should receive affection regardless
        assertEquals("lemmys", Json.getStringList(imp, "impersonal")[0])
    }

    @Test
    fun `affected stem should be affected based on overridden stem`(){
        val verb = CornishVerb(JSONObject()
            .put("normalForm", "dalleth")
            .put("stem", "dallath")
            .put("affection", "a/e")
        )

        assertEquals("dallath", verb.stem)
        assertEquals("dalleth", verb.getStem(Affection.RAISED))
    }

    @Test
    fun `affection should occur at the last vowel in the stem`(){
        val verb = CornishVerb(JSONObject()
            .put("normalForm", "mollethi")
            .put("affection", "o/e")
        )

        assertEquals("molloth", verb.stem)
        assertEquals("molleth", verb.getStem(Affection.RAISED))

        val pres = verb.conjugations.getJSONObject("present_future")
        assertEquals(listOf("mollothav"), pres.getStringList("singFirst"))
        assertEquals(listOf("mollethydh"), pres.getStringList("singSecond"))
    }

    @Test
    fun `impersonal conditional and subj imperfect should be raised`(){
        val verb = CornishVerb(JSONObject()
            .put("normalForm", "pregoth")
            .put("affection", "o/e")
        )

        assertEquals("pregoth", verb.stem)
        assertEquals("pregeth", verb.getStem(Affection.RAISED))

        val conj = verb.conjugations
        assertEquals(listOf("pregethsys"),
            conj.getJSONObject("conditional").getStringList("impersonal")
        )
        assertEquals(listOf("pregetthys"),
            conj.getJSONObject("subjunctive_imperfect").getStringList("impersonal")
        )
    }

    @Test
    fun `3rd personal should match the affection of the infinitive version`(){
        val verb = CornishVerb(JSONObject()
            .put("normalForm", "tewel")
            .put("affection", "a/e")
        )

        assertEquals("taw", verb.stem)
        assertEquals("tew", verb.getStem(Affection.RAISED))

        val pres = verb.conjugations.getJSONObject("present_future")
        assertEquals(listOf("tew"),
            pres.getStringList("singThird")
        )
    }

}

private fun JSONObject.getStringList(key: String): List<String>{
    return Json.getStringList(this, key)
}